#include "iostream.h" 
#include "min_breaks.h"

using namespace std;

int main()
{
    int length,breadth;
    cout<<"Enter the lenght and breadth of the chocolate bar: \n";
    cin>>length>>breadth;

    int breaks=0;

    breaks=min_breaks(length,breadth,breaks);

    cout<<"The minimum number of breaks taken  = "<<breaks;

    return 0;
}
