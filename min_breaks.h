int min_breaks(int length, int breadth, int breaks)
{
    if(length == 1 && breadth == 1)
    {
        return breaks;
    }
    breaks++;
    if(length > breadth)
    {
        breaks = min_breaks(length/2,breadth,breaks);
        breaks = min_breaks(length-length/2,breadth,breaks);
    }
    else
    {
        breaks = min_breaks(length, breadth/2,breaks);
        breaks = min_breaks(length,breadth-breadth/2,breaks);
    }
}
