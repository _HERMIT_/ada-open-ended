Student Name: Rohit Kumar Thakur
USN : 1SI18CS096

PROBLEM STATEMENT: 
Assume you have a chocolate bar consisting, as usual, 
of a number of squares arranged in a rectangular pattern.
Your task is to split the bar into small squares (always 
breaking along the lines between the squares) with a 
minimum number of breaks. How many will it take? 
Design and implement the solution to the above problem.